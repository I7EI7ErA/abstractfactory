interface AbstractSedan {
    getSedanBrand(): string;
}

class FordSedan implements AbstractSedan {
    getSedanBrand(): string {
        return 'Ford sedan';
    }
}

class ChevroletSedan implements AbstractSedan {
    getSedanBrand(): string {
        return 'Chevrolet sedan';
    }
}

interface AbstractPickup {
    getPickupBrand(): string;
}

class FordPickup implements AbstractPickup {
    getPickupBrand(): string {
        return 'Ford pickup';
    }
}

class ChevroletPickup implements AbstractPickup {
    getPickupBrand(): string {
        return 'Chervrolet pickup';
    }
}

interface AbstractCarsFactory {
    createSedan(): AbstractSedan;
    createPickup(): AbstractPickup;
}

class FordFactory implements AbstractCarsFactory {
    createSedan(): AbstractSedan {
        return new FordSedan();
    }

    createPickup(): AbstractPickup {
        return new FordPickup();
    }
}

class ChevroletFactory implements AbstractCarsFactory {
    createSedan(): AbstractSedan {
        return new ChevroletSedan();
    }

    createPickup(): AbstractPickup {
        return new ChevroletPickup();
    }
}

function client(carsFactory: AbstractCarsFactory) {
    const carSedan = carsFactory.createSedan();
    const carPickup = carsFactory.createPickup();

    console.log(carSedan.getSedanBrand());
    console.log(carPickup.getPickupBrand());
}

console.log('Ford factory:');
client(new FordFactory);

console.log('\nChevrolet factory:');
client(new ChevroletFactory);


