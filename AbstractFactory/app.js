class FordSedan {
    func() {
        return 'Ford sedan';
    }
}
class ChevroletSedan {
    func() {
        return 'Chevrolet sedan';
    }
}
class FordPickup {
    func() {
        return 'Ford pickup';
    }
}
class ChevroletPickup {
    func() {
        return 'Chervrolet pickup';
    }
}
class FordFactory {
    createSedan() {
        return new FordSedan;
    }
    createPickup() {
        return new FordPickup;
    }
}
class ChevroletFactory {
    createSedan() {
        return new ChevroletSedan;
    }
    createPickup() {
        return new ChevroletPickup;
    }
}
function client(carsFactory) {
    const carSedan = carsFactory.createSedan();
    const carPickup = carsFactory.createPickup();
    console.log(carSedan.func());
    console.log(carPickup.func());
}
console.log('Ford factory:');
client(new FordFactory);
console.log('\nChevrolet factory:');
client(new ChevroletFactory);
//# sourceMappingURL=app.js.map